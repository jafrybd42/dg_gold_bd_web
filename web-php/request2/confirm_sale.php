<?php

include_once("req_html_responec.php"); 
include_once("../third_party_server/server.php");
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$track_no = 0;

if(isset($_POST['track_no'])){
    $track_no = $_POST['track_no'];
}

$req_data = [
    "track_no" => $track_no
];

$server_obj = new Server();
$responce = $server_obj->post_req("/transaction/confirm_gold_to_money_track_no", $req_data, true);
$next_step = "";
$message = "";
$gold_price = 0;
$gold_amount = $VAT = $total = 0;
$otherInfo = array();

// print_r($responce);

if ($responce['success'] == 0 && ($responce['message'] == "Timeout Login Fast"  || $responce['message'] == "Unauthorize Request")) {
    echo 1990;
    return 1990;
}

if($responce != 0){
    if ($responce['success']  == 1){
        $next_step = $responce['next_step'];
        $message = $responce['message'];
        echo 0;
        return 0;
    } else {
        $next_step = $responce['next_step'];
        $message = $responce['message'];
    }
} else {
    $next_step = 'login-page';
    $message = 'Try again';
}


if($next_step != ""){

    $html_responce_object = new HTML_Responce();
    $html_responce = $html_responce_object->get_html_by_next_step_name($next_step, $message, "", "", $otherInfo);
    echo $html_responce;
} 
}

?>
