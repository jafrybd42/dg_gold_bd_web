<?php

include_once("req_html_responec.php"); 
include_once("../third_party_server/server.php");
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

$gold_amount = 0;
$cal_type = 1;

if(isset($_POST['gold_amount'])){
    $gold_amount = $_POST['gold_amount'];
}

if(isset($_POST['cal_type'])){
    $cal_type = $_POST['cal_type'];
}

$req_data = [
    "karat_type" => "22",
    "gold_amount" => $gold_amount,
    "cal_type" => $cal_type
];

$server_obj = new Server();
$responce = $server_obj->post_req("/transaction/check_gold_to_money", $req_data, true);
$next_step = "";
$message = "";
$gold_price = 0;
$gold_amount = $VAT = $total = 0;
$otherInfo = array();

// print_r($responce);

if ($responce['success'] == 0 && ($responce['message'] == "Timeout Login Fast"  || $responce['message'] == "Unauthorize Request")) {
    echo 1990;
    return 1990;
}

if($responce != 0){
    if ($responce['success']  == 1){
        $next_step = $responce['next_step'];
        $message = $responce['message'];
        $otherInfo['gold_amount'] = $responce['gold_amount'];
        $otherInfo['gold_price'] = $responce['gold_price'];
        $otherInfo['VAT'] = $responce['VAT'];
        $otherInfo['total'] = $responce['total'];
        $otherInfo['service_charge'] = $responce['service_charge'];
        $otherInfo['service_charge_details'] = $responce['service_charge_details'];
        $otherInfo['karat_type'] = $responce['karat_type'];
        $otherInfo['cal_type_main'] = $responce['cal_type'];
        $otherInfo['cal_type'] = $responce['cal_type'] == 1 ? " vori ( 1 vori = 11.664 gm)" : "gm ( 11.664 gm = 1 vori)";
    } else {
        $next_step = $responce['next_step'];
        $message = $responce['message'];
    }
} else {
    $next_step = 'login-page';
    $message = 'Try again';
}


if($next_step != ""){

    $html_responce_object = new HTML_Responce();
    $html_responce = $html_responce_object->get_html_by_next_step_name($next_step, $message, "", "", $otherInfo);
    echo $html_responce;
} 
}

?>
