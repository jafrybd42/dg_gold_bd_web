<div class="col l12">
    <div id="abtGoldHIW">
        <h3 class="componentsMainHeading" style="margin-top: 80px;">How it works</h3>
        <div class="hiw1Wrapper">
            <div id="abtGoldHIW">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#nav-homee" aria-controls="home" role="tab" data-toggle="tab">BUY</a></li>
                    <li role="presentation"><a href="#nav-salee" aria-controls="profile" role="tab" data-toggle="tab">Sale</a></li>
                </ul>


                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="nav-homee">
                        <div class="modal-body" id="div_modal_buy_idd">

                            <div class="hiw1Container">
                                <div class="hiw1Step">
                                    <div class="hiw1Stepper">
                                        <div class="hiw1Circle">
                                            <div class="hiw1Number">1</div>
                                        </div>
                                        <div class="hiw1Line"></div>
                                    </div>
                                    <div class="hiw1Content">
                                        <div class="hiw1H1">Enter the amount</div>
                                        <div class="hiw1Span">You can input your desired purchase amount in rupees or grams of gold.</div>
                                    </div>
                                </div>
                                <div class="hiw1Step">
                                    <div class="hiw1Stepper">
                                        <div class="hiw1Circle">
                                            <div class="hiw1Number">2</div>
                                        </div>
                                        <div class="hiw1Line"></div>
                                    </div>
                                    <div class="hiw1Content">
                                        <div class="hiw1H1">Buy in one-click</div>
                                        <div class="hiw1Span">Review your order and place order using UPI/Netbanking within the 5-minute price window.</div>
                                    </div>
                                </div>
                                <div class="hiw1Step">
                                    <div class="hiw1Stepper">
                                        <div class="hiw1Circle">
                                            <div class="hiw1Number">3</div>
                                        </div>
                                    </div>
                                    <div class="hiw1Content">
                                        <div class="hiw1H1">Secured and insured</div>
                                        <div class="hiw1Span">Your gold locker will be updated, where you can view your purchased Digital Gold holdings.</div>
                                    </div>
                                </div>
                            </div>

                            <div class="hiw1FixBottom">
                                <div class="lazyload-wrapper"><img class="" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/hiw.5dbff7ec.svg" alt="how it works" width="95" height="76"></div>
                            </div>
                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane" id="nav-salee">
                        <div class="modal-body" id="div_modal_sale_idd">

                            <div class="hiw1Container">
                                <div class="hiw1Step">
                                    <div class="hiw1Stepper">
                                        <div class="hiw1Circle">
                                            <div class="hiw1Number">1</div>
                                        </div>
                                        <div class="hiw1Line"></div>
                                    </div>
                                    <div class="hiw1Content">
                                        <div class="hiw1H1">Enter units to sell</div>
                                        <div class="hiw1Span">The current selling price will be shown.</div>
                                    </div>
                                </div>
                                <div class="hiw1Step">
                                    <div class="hiw1Stepper">
                                        <div class="hiw1Circle">
                                            <div class="hiw1Number">2</div>
                                        </div>
                                        <div class="hiw1Line"></div>
                                    </div>
                                    <div class="hiw1Content">
                                        <div class="hiw1H1">Sell in one-click</div>
                                        <div class="hiw1Span">Review and place your sell request within the 5-minute price window.</div>
                                    </div>
                                </div>
                                <div class="hiw1Step">
                                    <div class="hiw1Stepper">
                                        <div class="hiw1Circle">
                                            <div class="hiw1Number">3</div>
                                        </div>
                                    </div>
                                    <div class="hiw1Content">
                                        <div class="hiw1H1">Request Confirmed</div>
                                        <div class="hiw1Span">Your gold locker will be updated, where you can view your Digital Gold balance.</div>
                                    </div>
                                </div>
                            </div>

                            <div class="hiw1FixBottom">
                                <div class="lazyload-wrapper"><img class="" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/hiw.5dbff7ec.svg" alt="how it works" width="95" height="76"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>