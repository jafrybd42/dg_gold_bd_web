<?php
include_once("./third_party_server/server.php");

?>

<div class="col l4 ge44RightSideSticky">
    <div>
        <div>
            <div style="display: none; animation-duration: 300ms" class="rodal rodal-fade-leave" tabindex="-1">
                <div class="rodal-mask"></div>
                <div style="
                            width: 900px;
                            height: 240px;
                            animation-duration: 300ms;
                          " class="rodal-dialog rodal-zoom-leave">
                    <div class="child-wrapper lpw107LoginPopupWrapper">
                        <span class="rodal-close"></span>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($isLogin == 0) { ?>
            <div class="msc999MainDiv center-align undefined onMount-appear-done onMount-enter-done" style="width: 95%; height: auto">
                <div class="lazyload-wrapper">
                    <img class="" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/intro-gold.82acf847.svg" alt="Introducing Digital Gold Bangladesh Gold" width="130" height="87" />
                </div>
                <div class="msc999Heading">Introducing Digital Gold Bangladesh.</div>
                <div class="msc999Para">
                    Make your first digital gold purchase
                </div>
                <div class="">
                    <div class="btn51Btn btn51RipplePrimary btn51Primary" style="width: 100%; height: 45px; font-size: 14px">
                        <div class="absolute-center btn51ParentDimension">
                            <span class="absolute-center" style="padding: 0px 25px"><span>BUY NOW</span></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="msc999MainDiv center-align undefined onMount-appear-done onMount-enter-done" style="width: 95%; min-height: 220px">
                <div class="lazyload-wrapper">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#nav-home" aria-controls="home" role="tab" data-toggle="tab">BUY</a></li>
                        <li role="presentation"><a href="#nav-sale" aria-controls="profile" role="tab" data-toggle="tab">Sale</a></li>
                        <li role="presentation"><a href="#nav-contact" aria-controls="messages" role="tab" data-toggle="tab">Jwellery</a></li>
                    </ul>

                    <input type="hidden" value="<?php echo $gold_vori_price; ?>" id="gold_price">

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="nav-home">
                            <div class="modal-body" id="div_modal_buy_id">

                                <div class="form-group" style="display: flex;">
                                    <p>Money</p>
                                    <input style="width:70%; margin-left: 22%;" type="number" oninput="updateGoldCalculation('money', '1')" class="form-control" id="form_money_buy" name="form_money_buy" required>
                                </div>

                                <div class="form-group" style="display: flex;">
                                    <p>Gold</p>
                                    <div class="row" style="width:70%; margin-left: 22%;">
                                        <div class="col-sm-8">
                                            <input type="number" oninput="updateGoldCalculation('gold', '1')" class="form-control" id="form_gold_buy" name="form_gold_buy" required>
                                        </div>
                                        <div class="col-sm-4">
                                            <select name="gold_cal_type_buy" class="form-control" onchange="updateGoldCalculation('money', '1')" id="gold_cal_type_buy">
                                                <option value="1">Vori</option>
                                                <option value="2">gm</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <button type="button" onclick="check_buy()" class="btn btn-primary boxButton">Submit</button>

                            </div>
                        </div>


                        <div role="tabpanel" class="tab-pane" id="nav-sale">
                            <div class="modal-body" id="div_modal_sale_id">

                                <div class="form-group" style="display: flex;">
                                    <p>Gold</p>
                                    <div class="row" style="width:70%; margin-left: 22%;">
                                        <div class="col-sm-8">
                                            <input type="number" oninput="updateGoldCalculation('gold', '2')" class="form-control" id="form_gold_sale" name="form_gold_sale" required>
                                        </div>
                                        <div class="col-sm-4">
                                            <select name="gold_cal_type_sale" oninput="updateGoldCalculation('money', '2')" class="form-control" id="gold_cal_type_sale">
                                                <option value="1">Vori</option>
                                                <option value="2">gm</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" style="display: flex;">
                                    <p>Money</p>
                                    <input style="width:70%; margin-left: 22%;" type="number" oninput="updateGoldCalculation('money', '2')" class="form-control" id="form_money_sale" name="form_money_sale" required>
                                </div>

                                <button type="button" onclick="check_sale()" class="btn btn-primary boxButton">Submit</button>

                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="nav-contact">
                            <div class="modal-body" id="div_modal_buy_id">

                                <div align="center">
                                    <a href="./shop"><img src="./shop/images/logo.png" alt="vandor" height="80px" width="250px"></a>
                                </div>
                                <br>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        <?php } ?>

    </div>
</div>
</div>
</div>
</div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    function check_buy() {
        gold_amount = document.getElementById('form_gold_buy').value;
        cal_type = document.getElementById('gold_cal_type_buy').value;
        console.log("form_gold_buy: " + gold_amount);
        $.ajax({
            url: './request2/check_buy.php',
            type: 'POST',
            dataType: 'html',
            data: {
                gold_amount: gold_amount,
                cal_type: cal_type,
            },
            success: function(response) {
                console.log("successful");
                if (response == 1990) {
                    window.location = 'logout.php'
                } else {
                    $('#div_modal_buy_id').html(response);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }

        });
    }
</script>


<script>
    function check_buy2() {
        gold_amount = document.getElementById('form_gold_buy').value;
        karat_type = document.getElementById('form_karat_type_buy').value;
        cal_type = document.getElementById('gold_cal_type_buy').value;

        $.ajax({
            url: './request2/check_buy2.php',
            type: 'POST',
            dataType: 'html',
            data: {
                gold_amount: gold_amount,
                karat_type: karat_type,
                cal_type: cal_type
            },
            success: function(response) {
                console.log("successful");
                if (response == 1990) {
                    window.location = 'logout.php'
                } else {
                    $('#div_modal_buy_id').html(response);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }

        });
    }
</script>


<script>
    function confirm_buy() {
        track_no = document.getElementById('form_track_no_buy').value;
        $.ajax({
            url: './request2/confirm_buy.php',
            type: 'POST',
            dataType: 'html',
            data: {
                track_no: track_no,
            },
            success: function(response) {
                console.log("successful");
                console.log(response)
                if (response == 1990) {
                    window.location = 'logout.php'
                } else if (response == 0) {
                    window.location = 'explore.php'
                } else {
                    $('#div_modal_buy_id').html(response);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }
        });
    }
</script>

<script>
    function back_buy() {
        $.ajax({
            url: './request2/back_buy.php',
            type: 'POST',
            dataType: 'html',
            success: function(response) {
                $('#div_modal_buy_id').html(response);
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }

        });
    }
</script>

<script>
    function back_sale() {
        $.ajax({
            url: './request2/back_sale.php',
            type: 'POST',
            dataType: 'html',
            success: function(response) {
                $('#div_modal_sale_id').html(response);
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }

        });
    }
</script>


<script>
    function check_sale() {
        gold_amount = document.getElementById('form_gold_sale').value;
        cal_type = document.getElementById('gold_cal_type_buy').value;

        $.ajax({
            url: './request2/check_sale.php',
            type: 'POST',
            dataType: 'html',
            data: {
                gold_amount: gold_amount,
                cal_type: cal_type
            },
            success: function(response) {
                console.log("successful");
                if (response == 1990) {
                    window.location = 'logout.php'
                } else {
                    $('#div_modal_sale_id').html(response);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }

        });
    }
</script>


<script>
    function check_sale2() {

        gold_amount = document.getElementById('form_gold_sale').value;
        karat_type = document.getElementById('form_karat_type_sale').value;
        cal_type = document.getElementById('gold_cal_type_sale').value;

        $.ajax({
            url: './request2/check_sale2.php',
            type: 'POST',
            dataType: 'html',
            data: {
                gold_amount: gold_amount,
                karat_type: karat_type,
                cal_type: cal_type
            },
            success: function(response) {
                console.log("successful");
                if (response == 1990) {
                    window.location = 'logout.php'
                } else {
                    $('#div_modal_sale_id').html(response);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }

        });
    }
</script>


<script>
    function confirm_sale() {
        track_no = document.getElementById('form_track_no_sale').value;
        $.ajax({
            url: './request2/confirm_sale.php',
            type: 'POST',
            dataType: 'html',
            data: {
                track_no: track_no,
            },
            success: function(response) {
                console.log("successful");
                console.log(response);

                if (response == 1990) {
                    window.location = 'logout.php'
                } else if (response == 0) {
                    window.location = 'explore.php'
                } else {
                    $('#div_modal_sale_id').html(response);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }
        });
    }
</script>

<script>
    function removeLast0(num = 0) {

        let sNum = num.toString();

        if (sNum.includes(".") !== -1) {
            let sLength = sNum.length;

            for (i = sLength - 1; i > 0; i--) {
                if (sNum[i] == 0) {
                    sNum = sNum.slice(0, -1);
                } else break;
            }
            return Number(sNum);
        } else return num;
    }

    function updateGoldCalculation(input, type = 1) {
        console.log(type == 1)
        console.log(typeof(type))

        let goldField, moneyField, cal_type = "";
        if (type == 1) {
            goldField = document.getElementById("form_gold_buy");
            moneyField = document.getElementById("form_money_buy");
            cal_type = document.getElementById("gold_cal_type_buy");
            console.log(1)
        } else if (type == 2) {
            goldField = document.getElementById("form_gold_sale");
            moneyField = document.getElementById("form_money_sale");
            cal_type = document.getElementById("gold_cal_type_sale");
            console.log(2)
        } else {
            console.log(0)
            return 0;
        }

        console.log(3)
        let price = document.getElementById("gold_price").value;
        let money = moneyField.value;
        let gold = goldField.value;

        console.log("GOld: " + gold);
        money = money < 0 ? 0 : money;
        gold = gold < 0 ? 0 : gold;

        console.log("GOld 4: " + gold);

        // console.log(cal_type);
        if (input == "money") {
            if (cal_type.value == 1) {
                gold = (1 / price) * money;
                goldField.value = removeLast0(gold.toFixed(8));
            } else if (cal_type.value == 2) {
                gold = (11.664 / price) * money;
                goldField.value = removeLast0(gold.toFixed(8));
            }

        } else if (input == "gold") {
            if (cal_type.value == 1) {
                money = (price / 1) * gold;
                moneyField.value = Math.round(money);

            } else if (cal_type.value == 2) {
                money = (price / 11.664) * gold;
                moneyField.value = Math.round(money);
            }
        }
    }
</script>