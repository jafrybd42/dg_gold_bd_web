<section id="goldLandingFAQ">
    <h3 class="heading center-align">Frequenty Asked Questions</h3>
    <div class="fs567FaqWrapper">
        <div class="fs567FaqTopicDiv">
            <div class="fs567QuestionDiv">
                <div class="cur-po ">
                    <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                        <div class="acc11Title">
                            <div class="fs567Label"> What is Digital Gold?</div>
                        </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-2"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>
                    </div>
                    <div>
                        <div id="tab-2" class="panel-collapse collapse">
                            <div class="fs567Desc">
                                <div>Digital Gold is a convenient and cost-effective way of purchasing gold online. You can buy, sell, and accumulate gold of 99.90% purity in fractions anytime — all you need is your laptop/desktop and access to the internet.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567QuestionDiv">
                <div class="cur-po ">
                    <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                        <div class="acc11Title ">
                            <div class="fs567Label"> Where does Digital Gold Bangladesh get the gold from?</div>
                        </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-3"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>
                    </div>
                    <div>
                        <div id="tab-3" class="panel-collapse collapse">
                            <div class="fs567Desc">
                                <div>To provide you pure and top-quality bullion, Digital Gold Bangladesh has partnered with Augmont Goldtech Pvt. Ltd.— an integrated precious metals management company. All purchase/sell transactions of Digital Gold will be directly with Augmont Goldtech Pvt. Ltd. Augmont also has businesses in gold refining and manufacturing of tamper-proof packaged jewellery.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567QuestionDiv">
                <div class="cur-po ">
                    <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                        <div class="acc11Title ">
                            <div class="fs567Label"> What is the Gold Locker?</div>
                        </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-4"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>
                    </div>
                    <div>
                        <div id="tab-4" class="panel-collapse collapse">
                            <div class="fs567Desc">
                                <div>Gold Locker is a digital version of your holdings, wherein you can view your Digital Gold transactions.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567QuestionDiv">
                <div class="cur-po ">
                    <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                        <div class="acc11Title ">
                            <div class="fs567Label"> What is purity of the gold offered by Augmont?</div>
                        </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-5"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>
                    </div>
                    <div>
                        <div id="tab-5" class="panel-collapse collapse">
                            <div class="fs567Desc">
                                <div>Augmont offers 22-Karat gold of 999 purity (99.90% pure).</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567QuestionDiv">
                <div class="cur-po ">
                    <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                        <div class="acc11Title ">
                            <div class="fs567Label"> What are the charges applicable?</div>
                        </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-6"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>
                    </div>
                    <div>
                        <div id="tab-6" class="panel-collapse collapse">
                            <div class="fs567Desc">
                                <div>
                                    <div class="gf11Strong">Charges during purchase: </div>The live price is quoted on the basis of wholesale prices in the spot market. The gold rate excludes taxes, making charges and delivery charges. 3% GST is applicable while buying gold .<br>
                                    <div class="gf11Strong">Charges during sale: </div>When you are selling your gold back to Augmont, you can sell it back at the live price on a real time basis. There are no additional charges applicable.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567QuestionDiv">
                <div class="cur-po ">
                    <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                        <div class="acc11Title ">
                            <div class="fs567Label"> What are the taxes involved?</div>
                        </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-7"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>
                    </div>
                    <div>
                        <div id="tab-7" class="panel-collapse collapse">
                            <div class="fs567Desc">
                                <div>The gold rates shown on Digital Gold Bangladesh at the time of purchasing/selling is exclusive of taxes. However, tax on income from sale of gold is applicable.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>