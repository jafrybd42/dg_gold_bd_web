<div class="container web-align">
    <section id="goldFeatures">
        <div class="chip12Chip gf61FeatureChip"><span class="gf61FeatureChipText">FEATURES</span></div>
        <div class="pf61Wrapper">
            <div class="col l2 pf61MainHeading">Digital Gold on Digital Gold Bangladesh</div>
            <div class="col l2 pf61SubHeading para">Digital Gold is a convenient and cost-effective way of purchasing gold online. Stored in 100% secured vaults and insured, you can view your purchased Digital Gold in your gold locker, which is a digital version for your holdings.</div>
            <div class="pf61CollectionsWrapper">
                <div class="row">
                    <div class="col-sm-4 pf61CollectionsDiv">
                        <div class="col l2 pf61Icon">
                            <div class="lazyload-wrapper"><img class="is31Default" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/buy_small.713c11e1.svg" alt="Digital Gold Bangladesh" width="45" height="45"></div>
                        </div>
                        <div class="col l10 pf61Features">Buy small</div>
                        <div class="col l10 pf61Desc">Purchase gold in fractions for as low as 50 TK</div>
                    </div>
                    <div class="col-sm-4 pf61CollectionsDiv">
                        <div class="col l2 pf61Icon">
                            <div class="lazyload-wrapper"><img class="is31Default" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/sell_anytime.0200c8d1.svg" alt="Digital Gold Bangladesh" width="45" height="45"></div>
                        </div>
                        <div class="col l10 pf61Features">Sell anytime</div>
                        <div class="col l10 pf61Desc">Liquidate your Digital Gold and get your money in 2 days</div>
                    </div>

                    <div class="col-sm-4 pf61CollectionsDiv">
                        <div class="col l2 pf61Icon">
                            <div class="lazyload-wrapper"><img class="is31Default" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/exchange.3da169d4.svg" alt="Digital Gold Bangladesh" width="45" height="45"></div>
                        </div>
                        <div class="col l10 pf61Features">Exchange</div>
                        <div class="col l10 pf61Desc">Convert your Digital Gold to physical gold in the form of coins/bars/jewellery (coming soon)</div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-4 pf61CollectionsDiv">
                        <div class="col l2 pf61Icon">
                            <div class="lazyload-wrapper"><img class="is31Default" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/transparency.4953fe29.svg" alt="Digital Gold Bangladesh" width="45" height="45"></div>
                        </div>
                        <div class="col l10 pf61Features">Transparent</div>
                        <div class="col l10 pf61Desc">Live market price tracking and zero making charges</div>
                    </div>
                    <div class="col-sm-4 pf61CollectionsDiv">
                        <div class="col l2 pf61Icon">
                            <div class="lazyload-wrapper"><img class="is31Default" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/diversify.49d5d635.svg" alt="Digital Gold Bangladesh" width="45" height="45"></div>
                        </div>
                        <div class="col l10 pf61Features">Diversify</div>
                        <div class="col l10 pf61Desc">Balance your portfolio with Digital Gold to reduce risk concentration</div>
                    </div>

                    <div class="col-sm-4 pf61CollectionsDiv">
                        <div class="col l2 pf61Icon">
                            <div class="lazyload-wrapper"><img class="is31Default" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/safety_box.def6f52f.svg" alt="Digital Gold Bangladesh" width="45" height="45"></div>
                        </div>
                        <div class="col l10 pf61Features">100% secure</div>
                        <div class="col l10 pf61Desc">Stored in secured vaults and insured (verified by an independent trustee)</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>