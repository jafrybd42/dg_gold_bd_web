var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
const isEmpty = require("is-empty");
router.use(async function (req, res, next) {
    const token = req.headers['x-access-token'];

    if (token) {
        jwt.verify(token, global.config.secretKey,
            {
                algorithm: global.config.algorithm

            }, async function (err, decoded) {
                if (err) {
                    let errordata = {
                        message: err.message,
                        expiredAt: err.expiredAt
                    };
                    return res.send({
                        "success": false,
                        "message": "Timeout Login Fast"
                    });
                }
                req.decoded = decoded;
                //console.log("decoded : ");
                //console.log(decoded);
                next();
            });
    } else {
        return res.send({
            "success": false,
            "message": "Unauthorize Request"
        });
    }
});

module.exports = router;