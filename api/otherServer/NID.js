const isEmpty = require('is-empty');

let NID_list = [
    { "NID_NO": 928324842742, "Name": "Ashraful Islam Sheiblu", "Birthday": "1996-06-24" },
    { "NID_NO": 928324842743, "Name": "Saiful Islam", "Birthday": "1996-06-24" },
    { "NID_NO": 728324842724, "Name": "Riad", "Birthday": "1992-06-24" },
    { "NID_NO": 898324842744, "Name": "Rashed", "Birthday": "1992-06-24" },
    { "NID_NO": 128324842714, "Name": "Tanvir Shihab", "Birthday": "1992-06-24" },
    { "NID_NO": 828324845734, "Name": "Shihab Ahamed", "Birthday": "1992-06-24" },
    { "NID_NO": 128324823721, "Name": "Medina", "Birthday": "1992-06-24" },
    { "NID_NO": 128324823724, "Name": "Shovon", "Birthday": "1992-06-24" },
    { "NID_NO": 728324842727, "Name": "Jafry", "Birthday": "1992-06-24" },
    { "NID_NO": 728327542727, "Name": "Shohel", "Birthday": "1992-06-24" },
    { "NID_NO": 738327542727, "Name": "Alaya Begum", "Birthday": "1992-06-24" },
    { "NID_NO": 728327242727, "Name": "Jamal Uddin", "Birthday": "1992-06-24" },
];

let checkNID = async (NID = "") => {
    return new Promise((resolve, reject) => {
        let user = NID_list.find(nid => nid.NID_NO == NID);
        let result = {
            success: false,
            user_info: {}
        }
        if (!isEmpty(user)){
            result.success = true;
            result.user_info = user;
        }
        resolve(result)
    });
}


module.exports = {
    checkNID
}