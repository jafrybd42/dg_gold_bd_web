const express = require("express");
const router = express.Router();

const isEmpty = require("is-empty");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var path = require('path');
const multer = require('multer');

const admin_model = require('../model/admin');
const user_model = require('../model/user');
const user_balance_model = require('../model/balance');
const user_buy_sell_trangection_model = require('../model/buy_sell_trangaction');
const common_model = require('../model/common');
const verifyTokenWeb = require('../jwt/verify/verifyTokenWeb');

const url = "http://localhost:3001/admin_panel";


router.get(['/user-list'], async (req, res) => {

    let user_list = await user_model.getUserList();
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    return res.render('user_list', {
        message: "",
        page: "user-list",
        success: true,
        months: months,
        user_list: user_list
    });
});

router.get(['/user-profile/:id'], async (req, res) => {

    let user_id = parseInt(req.params.id);
    user_id = isNaN(user_id) ? 0 : user_id;

    console.log(user_id);
    

    let user_list = await user_model.getUserById(user_id);

    if (isEmpty(user_list)) {
        return res.render('404', {
            message: "Unknown User",
            page: "list"
        });
    }

    let user_balance = await user_balance_model.getUserBalanceByUserId(user_id);

    if (isEmpty(user_balance)) {
        user_balance = [{
            "id": 0,
            "user_id": user_id,
            "money_balance": 0,
            "gold_18_karat": 0,
            "gold_21_karat": 0,
            "gold_22_karat": 0,
            "updated_at": "1999-01-01",
        }]
    }else {
        user_balance = user_balance[0];
    }

    let user_buy_sell_trangection = await user_buy_sell_trangection_model.getUserTransactionByUserId(user_id);

    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return res.render('user_profile', {
        message: "",
        page: "user-list",
        user_details: user_list[0],
        user_balance: user_balance,
        user_buy_sell_trangection: user_buy_sell_trangection,
        months: months,
        success: true
    });
});

router.get('/*', (req, res) => {
    return res.render('404', {
        message: "Page Not Found",
        page: "user-list"
    });
});


router.post('/*', (req, res) => {
    return res.render('404', {
        message: "Page Not Found",
        page: "user-list"
    });
});



module.exports = router;