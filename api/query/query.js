const isEmpty = require("is-empty");


//Admin 

let getAdminByEmail = () => {
    return "SELECT * FROM `dgb_admins` where email = ? "
}

let addNewAdmin = () => {
    return "INSERT INTO `dgb_admins` SET ? ";
}


// User 

let getUserByPhoneOrEmail = () => {
    return "SELECT * FROM `dgb_users` where ( phone_number = ? or email = ? )";
}

let getUserByEmail = () => {
    return "SELECT * FROM `dgb_users` where  email = ? ";
}

let getUserByEmailOrPhone = () => {
    return "SELECT * FROM `dgb_users` where  email = ? or phone = ?";
}

let getUserByNID = () => {
    return "SELECT * FROM `dgb_users` where  nid = ? ";
}

let registerUserAccount = () => {
    return "INSERT INTO `dgb_users` SET ? ";
}

let getUserById = () => {
    return "SELECT id, name, email, phone, nid, is_verified, created_at, status FROM `dgb_users` where  id = ?";
}


let getUserList = () => {
    return "SELECT id, name, email, phone, nid, is_verified, created_at, status FROM `dgb_users`";
}


let updateUserStatusByID = () => {
    return "UPDATE `dgb_users` set status = ? where id = ?";
}

let updateProfileByID = () => {
    return "UPDATE `dgb_users` set name = ? where id = ?";
}

let updateUser_PasswordByID = () => {
    return "UPDATE `dgb_users` set password = ? where id = ?";
}

// Balance
let getUserBalanceByUserId = () => {
    return "SELECT * FROM `dgb_balances` where user_id = ?";
}

let addNewBalance = () => {
    return "insert into `dgb_balances` SET ?"
}

let updateBalanceById = (data) => {

    let keys = Object.keys(data);
    let query = "update `dgb_balances` set " + keys[0] + " = ? ";

    for (let i = 1; i < keys.length; i++) {
        query += ", " + keys[i] + " = ? ";
    }

    query += " where id = ?";
    return query;
}

let updateBalanceByUserIdAndKaratType = (karate_type) => {
    let query = " UPDATE `dgb_balances` set ";
    if (karate_type == 'gold_22_karat') {
        query += " gold_22_karat = ? ";
    } else if (karate_type == 'gold_21_karat') {
        query += " gold_21_karat = ? ";
    } else {
        query += " gold_18_karat = ? ";
    }
    return query + " where user_id = ?";
}



// Transaction
let addNewBuyAndSaleTransaction = () => {
    return "insert into `dgb_buy_sell_transaction` SET ?"
}

let getUserBuyAndSaleTransactionByUserId = () => {
    return "SELECT * FROM `dgb_buy_sell_transaction` where user_id = ? order by created_at DESC ";
}

let getBuyCountByUser_idGold_typeAndDateRange = () => {
    return "SELECT sum(total_gold) as total_gold FROM `dgb_buy_sell_transaction` WHERE user_id = ? and from_currency_type = 'money' and gold_type = ? and created_at <= ? and created_at >= ?";
}



// Temp Transaction
let addNewTempBuyAndSaleTransaction = () => {
    return "insert into `dgb_temp_buy_sell_transaction` SET ?"
}

let getUserTempBuyAndSaleTransactionByUserId = () => {
    return "SELECT * FROM `dgb_temp_buy_sell_transaction` where user_id = ? order by created_at DESC ";
}

let getUserTempBuyAndSaleTransactionByTrackNo = () => {
    return "SELECT * FROM `dgb_temp_buy_sell_transaction` where track_no = ? order by created_at DESC ";
}

let updateStatusOfTempBuySellByTrack_No = () => {
    return "UPDATE `dgb_temp_buy_sell_transaction` set status = 0 where track_no = ?";
}


// Gold Type


let getGoldTypeByName = () => {
    return "SELECT * FROM `dgb_gold_types` where karat_title = ?";
}
let getGoldTypeById = () => {
    return "SELECT * FROM `dgb_gold_types` where id = ?";
}

let updateGoldPriceTypeByID = () => {
    return "UPDATE `dgb_gold_types` SET vori_price = ? , gram_price =  ?, updated_at = ? WHERE id = ?"
}

//Gold Type Price history

let getGoldTypePriceHistoryListByGold_type_ID = () => {
    return "SELECT * FROM `dgb_gold_type_prices_history` where dgb_gold_types_id = ? order by incress_date DESC ";
}

let updateGoldTypePriceHistoryByID = () => {
    return "UPDATE `dgb_gold_type_prices_history` SET vori_price = ? , gram_price =  ?, incress_date = ? WHERE id = ?"
}

let addNewGoldPriceTypeHistory = () => {
    return "insert into `dgb_gold_type_prices_history` SET ?";
}

// token 

let addNewToken = () => {
    return "insert into `dgb_tokens` SET ?"
}

let getTokenBySend_boxAndTokenGenerateReason = () => {
    return "SELECT * FROM `dgb_tokens` where send_box = ? and token_generate_reson = ? ";
}

let getTokenBySend_boxAndtrack_no = () => {
    return "SELECT * FROM `dgb_tokens` where send_box = ? and track_no = ? ";
}

let getTokenByTrack_no = () => {
    return "SELECT * FROM `dgb_tokens` where  track_no = ? ";
}

let deleteTokenByTrack_no = () => {
    return "DELETE FROM `dgb_tokens` where  track_no = ? ";
}





// Registration Request

let getReqRequestByID = () => {
    return "SELECT * FROM `dgb_reg_request` where email = ? ";
}

let getReqRequestByTrackNo = () => {
    return "SELECT * FROM `dgb_reg_request` where track_no = ? ";
}

let addNewReqRequest = () => {
    return "INSERT INTO `dgb_reg_request` SET ? ";
}

let deleteReqRequestByTrack_no = () => {
    return "DELETE FROM `dgb_reg_request` where track_no = ? ";
}

// Product 

let getProductList = () => {
    return "SELECT * FROM `dgb_jewellery_vendor_products` where status = 1 ";
}

let getProductByID = () => {
    return "SELECT * FROM `dgb_jewellery_vendor_products` where status = 1 and id = ? ";
}

//  jewellery cart
let getCartJwelleryListByUserId = () => {
    return "SELECT * FROM `dgb_jewellery_vendor_product_carts` where user_id = ?";
}

let addNewCartJwellery = () => {
    return "insert into `dgb_jewellery_vendor_product_carts` SET ?"
}

let getCartJwelleryByUser_IdAndProduct_Id = () => {
    return "SELECT * FROM `dgb_jewellery_vendor_product_carts` where user_id = ? and product_id = ? ";
}


let getCartJwelleryByUser_IdAndCart_Id = () => {
    return "SELECT * FROM `dgb_jewellery_vendor_product_carts` where user_id = ? and id = ? ";
}

let updateCartJwelleryQuentityById = () => {
    return "Update `dgb_jewellery_vendor_product_carts` set quantity = ? where id = ? ";
}

let removeCartJwelleryById = () => {
    return 'DELETE FROM `dgb_jewellery_vendor_product_carts` where id = ?';
}

let removeAllCartItemByUserId = () => {
    return 'DELETE FROM `dgb_jewellery_vendor_product_carts` where user_id = ?';
}

// jwellery_buy

let addNewJwellery_buy = () => {
    return "insert into `dgb_jwellery_buy` SET ?"
}

module.exports = {

    //Admin 
    getAdminByEmail,
    addNewAdmin,

    // User 
    getUserByPhoneOrEmail,
    getUserByEmail,
    registerUserAccount,
    getUserById,
    getUserList,
    updateUserStatusByID,
    updateProfileByID,
    updateUser_PasswordByID,
    getUserByNID,
    getUserByEmailOrPhone,

    // Balance
    getUserBalanceByUserId,
    addNewBalance,
    updateBalanceByUserIdAndKaratType,
    updateBalanceById,

    //Transaction
    getUserBuyAndSaleTransactionByUserId,
    addNewBuyAndSaleTransaction,
    getBuyCountByUser_idGold_typeAndDateRange,

    // Temp Buy sell transaction 
    addNewTempBuyAndSaleTransaction,
    getUserTempBuyAndSaleTransactionByUserId,
    getUserTempBuyAndSaleTransactionByTrackNo,
    updateStatusOfTempBuySellByTrack_No,

    //Gold Type
    getGoldTypeByName,
    getGoldTypeById,
    updateGoldPriceTypeByID,

    //Gold Type Price history
    getGoldTypePriceHistoryListByGold_type_ID,
    updateGoldTypePriceHistoryByID,
    addNewGoldPriceTypeHistory,

    // token 
    addNewToken,
    getTokenBySend_boxAndTokenGenerateReason,
    getTokenBySend_boxAndtrack_no,
    getTokenByTrack_no,
    deleteTokenByTrack_no,

    // Registration Request
    getReqRequestByID,
    addNewReqRequest,
    getReqRequestByTrackNo,
    deleteReqRequestByTrack_no,

    // Product 
    getProductList,
    getProductByID,

    // jewellery cart
    getCartJwelleryListByUserId,
    addNewCartJwellery,
    getCartJwelleryByUser_IdAndProduct_Id,
    updateCartJwelleryQuentityById,
    getCartJwelleryByUser_IdAndCart_Id,
    removeCartJwelleryById,
    removeAllCartItemByUserId,

    // jwellery_buy
    addNewJwellery_buy

}