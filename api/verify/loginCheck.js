const express = require('express');
const router = express.Router();
var url = "http://localhost:3001/admin_panel";

router.use(function (req, res, next)  {

    if(req.session.is_login_admin){
        next();
    } else {
        res.redirect(url + '/login');
    }
});

module.exports = router;