const express = require("express");
const router = express.Router();

const isEmpty = require("is-empty");
const uuidv4 = require("uuid");
const jwt = require('jsonwebtoken');


const user_model = require("../../model/user");
const balance_model = require("../../model/balance");
const common_model = require("../../model/common");
const token_model = require("../../model/token");
const reg_request_model = require("../../model/reg_request");


const verifyToken = require("../../jwt/verify/api_v1/verifyToken");

router.post('/log_reg', async (req, res) => {
    let email = req.body.email;
    let isError = 0;
    let errorMessage = "";

    console.log(email);
    let isEmailReq = 1;

    // Check Email number validation
    if (email === undefined || isEmpty(email)) {
        isError = 1;
        errorMessage += "Give valid phone number or email.";
    } else if (!isNaN(email)) {
        if (email.length != 11) {
            isError = 1;
            errorMessage += "Give valid phone number.";
        } else {
            isEmailReq = 0;
        }
    } else {
        const re = /\S+@\S+\.\S+/;
        tryToLoginBy = "email";
        if (!re.test(email)) {
            isError = 1;
            errorMessage += " Give a valid email.";
        }
    }

    if (isError == 1) {
        return res.status(200).send({
            "status": 400,
            'message': errorMessage,
            "success": false,
            "next_step": "login-page"
        })
    }

    let userList = await user_model.getUserByEmailOrPhone(email);
    let OTP = await common_model.rendomDigitNumberGenerate();
    let hash_OTP = await common_model.hashAString(OTP);

    let todayTime = await common_model.getTodayDateTime();
    let expired_time = await common_model.getCustomDate(todayTime, 1, 0, 0);

    let track_no = uuidv4();

    let token_date = {
        "otp": hash_OTP,
        "send_to": isEmailReq == 1 ? "email" : "phone",
        "send_box": email,
        "user_id": 0,
        "token_generate_reson": "registration",
        "track_no": track_no,
        "expired_time": expired_time,
        "created_at": todayTime
    }

    if (!isEmpty(userList)) {
        token_date.user_id = userList[0].id;
        token_date.token_generate_reson = "login";
    }

    await token_model.addNewToken(token_date);
    if (isEmailReq == 1) {
        let email_send = await common_model.sendEmail(email, "OTP FOR LOGIN" , "Loging OTP is " + OTP + " . Never Share your OTP.");
    } else {
        let send_sms = await common_model.sendSMS(email, "Your OTP no is: " + OTP + " . Never Share your OTP.");
    }
    console.log("OTP: " + OTP);

    let message_show = isEmailReq == 1 ? "email" : "phone" ;
    return res.status(200).send({
        'status': 200,
        'message':  "Check Your " +  message_show + " For OTP",
        "success": true,
        "track_no": track_no,
        "email": req.body.email,
        "next_step": "check-OTP"
    });

});

router.post('/otp_check', async (req, res) => {
    let reqObject = {
        'track_no': req.body.track_no,
        'given_otp': req.body.otp
    }

    // console.log(reqObject)

    // check token exit or not
    let token_date = await token_model.getTokenByTrack_no(reqObject.track_no);

    if (isEmpty(token_date)) {
        return res.status(200).send({
            'status': 200,
            'message': "Wrong request as unknown Track ID",
            "success": false,
            "next_step": "login-page"
        });
    } else {
        // when yes, Then match OTP & track NO 
        isOTPMatch = await common_model.compareDataWithHash(reqObject.given_otp, token_date[0].otp);

        if (isOTPMatch) {
            const toDayTime = await common_model.getTodayDateTime();
            const isDateNotExpired = await common_model.compareTwoDate(token_date[0].expired_time, toDayTime);

            if (isDateNotExpired) {
                if (token_date[0].user_id == 0) {  // They request for Registration
                    if (token_date[0].token_generate_reson === "registration") {

                        let res_req_date = {
                            "status": 1,
                            "track_no": reqObject.track_no,
                            "phone": "",
                            "email": ""
                        }

                        if (token_date[0].send_to === "email") {
                            res_req_date.email = token_date[0].send_box;
                        } else {
                            res_req_date.phone = token_date[0].send_box;
                        }

                        let res_reqSaveResult = await reg_request_model.addNewReqRequest(res_req_date);

                        return res.status(200).send({
                            'status': 200,
                            'message': "OTP is Currect",
                            "success": true,
                            "next_step": "NID-page"
                        });
                    } else {
                        return res.status(200).send({
                            'status': 200,
                            'message': "Wrong Request 00",
                            "success": false,
                            "next_step": "login-page"
                        });
                    }
                } else { // They request for Login

                    if (token_date[0].token_generate_reson !== "login") {
                        // work in future
                    }
                    let user_info = await user_model.getUserById(token_date[0].user_id);

                    if (isEmpty(user_info)) { // user unknown
                        return res.status(200).send({
                            'status': 200,
                            'message': "Unknown User",
                            "success": false,
                            "next_step": "login-page"
                        });
                    } else {
                        if (user_info[0].status == 1) {
                            let token_info = {
                                "user_name": user_info[0].name,
                                "user_id": user_info[0].id,
                                "user_nid": user_info[0].nid,
                                "user_email": user_info[0].email,
                                "user_phone": user_info[0].phone,
                            }

                            //  "Generate Token"
                            let token = jwt.sign(token_info, global.config.secretKey, {
                                algorithm: global.config.algorithm,
                                expiresIn: '1440m' // one day
                            });

                            let user_info_responce = {
                                user_name: user_info[0].name,
                                user_email: user_info[0].email
                            }

                            return res.status(200).send({
                                'status': 200,
                                'message': "Successfully login this System.",
                                "success": true,
                                "next_step": "home-page",
                                "user_info": user_info_responce,
                                "token": token
                            });

                        } else {
                            return res.status(200).send({
                                'status': 200,
                                'message': "Your account is Deactive. Please Contract to Digital Gold Support Team.",
                                "success": true,
                                "next_step": "login-page"
                            });
                        }
                    }
                }
            } else {
                return res.status(200).send({
                    'status': 200,
                    'message': "OTP Expired",
                    "success": false,
                    "next_step": "login-page"
                });
            }
        } else {
            return res.status(200).send({
                'status': 200,
                'message': "Wrong OTP",
                "success": false,
                "next_step": "re-check-OTP-page"
            });
        }
    }


});

router.post('/nid_check', async (req, res) => {
    let NID = req.body.NID;
    let email = req.body.email;
    let track_no = req.body.track_no;

    console.log(email);

    let isError = 0;
    let errorMessage = "";
    let isEmailReq = 1;

    let token_date = await token_model.getTokenByTrack_no(track_no);

    if (isEmpty(token_date)) {
        return res.status(200).send({
            'status': 200,
            'message': "Wrong request as unknown Track ID",
            "success": false,
            "next_step": "login-page"
        });
    }

    // Check NID  validation
    if (NID === undefined || isEmpty(NID)) {
        return res.status(200).send({
            'message': "Give a valied NID",
            "success": false,
            "nid": req.body.nid,
            "next_step": "give_another_nid"
        })
    }

    let userListNID = await user_model.getUserByNID(NID);
    if (!isEmpty(userListNID)) {
        return res.status(200).send({
            'status': 200,
            'message': "NID Already Used.",
            "success": false,
            "nid": req.body.nid,
            "next_step": "give_another_nid"
        });
    } else {

        let NID_INFO = await require("../../otherServer/NID").checkNID(NID);

        if (NID_INFO.success === false) {
            return res.status(200).send({
                'status': 200,
                'message': "NID Not Found in server",
                "success": false,
                "next_step": "give_another_nid"
            });
        } else {

            let todayTime = await common_model.getTodayDateTime();
            let reg_req_list = await reg_request_model.getReqRequestByTrackNo(track_no);

            if (isEmpty(reg_req_list)) {
                return res.status(200).send({
                    'status': 200,
                    'message': "Step Missing",
                    "success": false,
                    "next_step": "login-page"
                });
            }

            // Check Email number validation
            if (email === undefined || isEmpty(email)) {
                isError = 1;
                errorMessage += "Give valid email or Phone NO.";
            } else if (!isNaN(email)) {
                if (email.length != 11) {
                    isError = 1;
                    errorMessage += "Give valid phone number.";
                } else {
                    isEmailReq = 0;
                }
            } else {
                const re = /\S+@\S+\.\S+/;
                if (!re.test(email)) {
                    isError = 1;
                    errorMessage += " Give a valid email.";
                }
            }

            // else {
            //     const re = /\S+@\S+\.\S+/;
            //     tryToLoginBy = "email";
            //     if (!re.test(email)) {
            //         isError = 1;
            //         errorMessage += " Give a valid email.";
            //     }
            // }

            if (isError == 1) {
                return res.status(200).send({
                    "status": 400,
                    'message': errorMessage,
                    "success": false,
                    "next_step": "login-page"
                })
            }

            let userListByEmail = await user_model.getUserByEmailOrPhone(email);

            if (!isEmpty(userListByEmail)) {
                return res.status(200).send({
                    "status": 400,
                    'message': "Email Or Phone no already used.",
                    "success": false,
                    "next_step": "login-page"
                })
            }

            let new_user = {
                "name": NID_INFO.user_info.Name,
                "email": isEmailReq == 1 ? email : "",
                "phone": isEmailReq == 0 ? email : "",
                "nid": NID_INFO.user_info.NID_NO,
                "is_verified": 1,
                "created_at": todayTime,
                "updated_at": todayTime
            }

            let user_entry_result = await user_model.addNewUser(new_user);

            let token_info = {
                "user_name": new_user.name,
                "user_id": user_entry_result.insertId,
                "user_nid": new_user.nid,
                "user_email": new_user.email,
                "user_phone": new_user.phone,
            }

            //  "Generate Token"
            let token = jwt.sign(token_info, global.config.secretKey, {
                algorithm: global.config.algorithm,
                expiresIn: '1440m' // one day
            });

            let user_info_responce = {
                user_name: token_info.user_name,
                user_email: token_info.user_email
            }

            await token_model.deleteTokenByTrack_no(track_no);
            await reg_request_model.deleteReqRequestByTrack_no(track_no);

            await balance_model.addNewBalance({ "user_id": token_info.user_id });

            if (isEmailReq == 1) {
                let email_send = await common_model.sendEmail(email, "Congratulation For Login" , "Congratulation "+ token_info.user_name +", Your Account Successfully Create. Welcome to Digital Gold Bangladesh.");
            } else {
                let send_sms = await common_model.sendSMS(email, "Congratulation "+ token_info.user_name +", Your Account Successfully Create. Welcome to Digital Gold Bangladesh.");
            }

            return res.status(200).send({
                'status': 200,
                'message': "Successfully register this System.",
                "success": true,
                "next_step": "home-page",
                "user_info": user_info_responce,
                "token": token
            });
        }
    }
});

router.post('/balance', verifyToken, async (req, res) => {
    let user_id = req.decoded.user_id;
    let balance = await balance_model.getUserBalanceByUserId(user_id);
    return res.send({
        "success": true,
        "message": "",
        "data": balance[0]
    });
});

router.get('/', (req, res) => {
    return res.send({
        "success": true,
        "message": "",
        "api v": 1
    });
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});



module.exports = router;