const express = require("express");
const isEmpty = require("is-empty");
const router = express.Router();

const product_model = require("../../model/product");

router.get('/list', async (req, res) => {
    let product_list = await product_model.getProductList();
    return res.send({
        "success": true,
        "product_list": product_list,
        "message": "",
    });
});

router.get('/details', async (req, res) => {
    let product_id = req.query.product_id ?? 0;
    let product_details = await product_model.getProductByID(product_id);

    return res.send({
        "success": true,
        "message": "",
        "details": isEmpty(product_details) ? {} : product_details[0]
    });
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});



module.exports = router;