var fs = require('fs');
const isEmpty = require('is-empty');
const moment = require("moment");
const bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
const { text } = require('body-parser');
var request = require('request');

const balance_model = require("./balance");
const buy_sell_trangaction_model = require("./buy_sell_trangaction");

// var transporter = nodemailer.createTransport({
//     service: 'gmail',
//     auth: {
//         user: 'y4tube4@gmail.com',
//         pass: '11223344665'
//     }
// });


let deleteFile = async (path, fileName) => {
    try {
        fs.unlinkSync(path + fileName);
        return { success: true, message: "file Delete" };

    } catch (err) {
        console.error(err)
        return { success: false, message: "file Delete Fail" };
    }
}

let getTodayDateTime = async () => {
    let date = new Date(new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Dhaka'
    }));

    let toDayDateWithTime = date.getFullYear() + '-' +
        ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
        ('00' + (date.getDate())).slice(-2) + ' ' +
        ('00' + date.getHours()).slice(-2) + ':' +
        ('00' + date.getMinutes()).slice(-2) + ':' +
        ('00' + date.getSeconds()).slice(-2);

    return toDayDateWithTime;
}



let addFiveMinutes = async () => {

    let date = new Date(new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Dhaka'
    }));

    var minutesToAdd = 30;

    let addFiveMinutes = date.getFullYear() + '-' +
        ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
        ('00' + (date.getDate())).slice(-2) + ' ' +
        ('00' + date.getHours()).slice(-2) + ':' +
        ('00' + date.getMinutes()).slice(-2) + ':' +
        ('00' + date.getSeconds()).slice(-2);

    return addFiveMinutes;
}

let getTodayDate = async () => {
    let date = new Date(new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Dhaka'
    }));

    let toDayDate = date.getFullYear() + '-' +
        ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
        ('00' + (date.getDate())).slice(-2);

    return toDayDate;
}


let getCustomDate = async (date = "20/12/2012", extraDay = 0, extraMonth = 0, extraYear = 0) => {
    try {
        let customDate = new Date(date);
        customDate.setDate(customDate.getDate() + (extraDay));
        customDate.setMonth(customDate.getMonth() + (extraMonth));
        customDate.setFullYear(customDate.getFullYear() + (extraYear));

        date = customDate.getFullYear() + '-' +
            ('00' + (customDate.getMonth() + 1)).slice(-2) + '-' +
            ('00' + (customDate.getDate())).slice(-2);
        return date;

    } catch (error) {
        return await getTodayDate();
    }
}

let getFormatedDateForWebView = async (date = "20/12/2012") => {
    try {
        let customDate = new Date(date);
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        date = ('00' + (date.getDate())).slice(-2) + ' ' + months[date.getMonth()] + ', ' + date.getFullYear();
        return date;

    } catch (error) {
        return await getTodayDate();
    }
}


let compareTwoDate = async (date1 = "2012-12-12", date2 = "2012-12-1", want_true_false = true) => {
    // give true if date1 is getter then or equal
    //  date1 > date2  then true
    try {
        const fast_date = moment(await getCustomDate(date1)); // formate the data and convert ISO Formate
        const last_date = moment(await getCustomDate(date2));
        const duration = moment.duration(fast_date.diff(last_date));
        const days = duration.asDays();
        // console.log(days)
        if (want_true_false === true) {
            return days > -1 ? true : false;
        } else {
            return days;
        }
    } catch (error) {
        return 0;
    }
}

let rendomDigitNumberGenerate = async () => {
    return Math.floor(100000 + Math.random() * 900000);
}

let hashAString = async (stringText = "Hash is a Goodthing") => {
    stringText = "" + stringText;
    const hashString = bcrypt.hashSync(stringText, 10);
    return hashString;
}

let sendEmail = async (emailAddress = "", subject = "No Subject", test = "") => {

    return new Promise((resolve, reject) => {
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'y4tube4@gmail.com',
                pass: '11223344665'
            }
        });

        var mailOptions = {
            from: {
                name: 'Digital Gold Bd',
                address: 'y4tube4@gmail.com'
            },
            to: emailAddress,
            subject: subject,
            html: '<h1>' + test + '</h1>'
        };

        transporter.sendMail(mailOptions, async function (error, info) {
            if (error) {
                resolve(false);
            } else {
                resolve(true);
            }
        });
    })
}

let getDateTimeFormateForDatabase = async (date1 = "2012-12-12") => {
    let date = new Date(date1);

    let toDayDateWithTime = date.getFullYear() + '-' +
        ('00' + (date.getMonth() + 1)).slice(-2) + '-' +
        ('00' + (date.getDate())).slice(-2) + ' ' +
        ('00' + date.getHours()).slice(-2) + ':' +
        ('00' + date.getMinutes()).slice(-2) + ':' +
        ('00' + date.getSeconds()).slice(-2);

    return toDayDateWithTime;
}

let checkEmailValid = async (email = "0") => { // formate check true = currect formate, false = wrong formate

    const re = /\S+@\S+\.\S+/;
    if (!re.test(email)) {
        return false;
    }
    return true;
}

let compareDataWithHash = async (generalData = "43535", hashData = "546b3dtve4 ") => { // return true false
    try {
        if (bcrypt.compareSync(generalData, hashData)) {
            return true;
        } else {
            return false;
        }
    } catch (error) {
        return false;
    }
}

let getCurrentVoryWightInGram = async () => {
    return 11.664;
}

let getVoriTOGram = async (vory = 1) => {
    return vory * await getCurrentVoryWightInGram();
}

let getGramWidrowLimitInGram = async () => {
    return {
        "MIN": 1,
        "MAX": 20000
    };
}

let getVATPercentis = async () => {
    return 5;
}

let goldToTakaConvert = async (goldAmount = 0, vori_price = 0, cal_type = 1) => { //cal_type 1 =vori,  2 = gm
    try {
        if (cal_type == 1) {
            return goldAmount * vori_price;
        } else if (cal_type == 2) {
            return Math.round((vori_price / await getVoriTOGram()) * goldAmount);
        } else {
            return 0;
        }
    } catch (error) {
        return 0;
    }
}

let gold_available_balance_by_month = async (user_id = 0, gold_type = 0) => {
    let balance = await balance_model.getUserBalanceByUserId(user_id);

    if (isEmpty(balance)) {
        await balance_model.addNewBalance({ "user_id": temp_buy_sell_info[0].user_id });
        balance = await balance_model.getUserBalanceByUserId(user_id);
    }


    let today = await getTodayDate();
    let customData1 = await getCustomDate(today, 1, 0, 0); //today;
    let customData2 = await getCustomDate(customData1, 0, -3, 0);

    let gold_balance = {
        "gold_type": gold_type,
        "balance_last_3_month": 0,
        "balance_last_3_month_duration": {},
        "balance_last_6_month": 0,
        "balance_last_6_month_duration": {},
        "balance_other": 0,
        "balance_other_duration": {}
    }

    // Generate Last Insert Balance By last 3 , last 6 and other month
    let total_buy_count = await buy_sell_trangaction_model.getBuyCountByUser_idGold_typeAndDateRange(user_id, gold_type, customData1, customData2);
    gold_balance.balance_last_3_month = total_buy_count[0].total_gold ?? 0;
    gold_balance.balance_last_3_month_duration.start_date = customData2;
    gold_balance.balance_last_3_month_duration.end_date = customData1;


    customData1 = customData2; //await getCustomDate(customData2, 0, 0, 0);
    customData2 = await getCustomDate(customData1, 0, -3, 0);


    total_buy_count = await buy_sell_trangaction_model.getBuyCountByUser_idGold_typeAndDateRange(user_id, gold_type, customData1, customData2);

    gold_balance.balance_last_6_month = total_buy_count[0].total_gold ?? 0;
    gold_balance.balance_last_6_month_duration.start_date = customData2;
    gold_balance.balance_last_6_month_duration.end_date = customData1;



    customData1 = customData2; // await getCustomDate(customData2, 0, 0, 0);
    total_buy_count = await buy_sell_trangaction_model.getBuyCountByUser_idGold_typeAndDateRange(user_id, gold_type, customData1, "2020-01-01");

    gold_balance.balance_other = total_buy_count[0].total_gold ?? 0;
    gold_balance.balance_other_duration.start_date = "2020-01-01";
    gold_balance.balance_other_duration.end_date = customData1;

    // calculate Available balance
    let user_balance = 0;
    if (gold_type == 1) {
        user_balance = balance[0].gold_22_karat;
    } else if (gold_type == 2) {
        user_balance = balance[0].gold_21_karat;
    } else if (gold_type == 3) {
        user_balance = balance[0].gold_18_karat;
    }

    // console.log(user_balance);
    // console.table(gold_balance);

    // compare available balance 
    if (gold_balance.balance_last_3_month <= user_balance) {
        user_balance = user_balance - gold_balance.balance_last_3_month;
    } else {
        gold_balance.balance_last_3_month = user_balance;
        gold_balance.balance_last_6_month = 0;
        gold_balance.balance_other = 0;
        user_balance = 0;
    }

    if (gold_balance.balance_last_6_month <= user_balance) {
        user_balance = user_balance - gold_balance.balance_last_6_month;
    } else {
        gold_balance.balance_last_6_month = user_balance;
        gold_balance.balance_other = 0;
        user_balance = 0;
    }

    gold_balance.balance_other = user_balance;

    // console.table(gold_balance);

    return gold_balance;
}


let calculate_service_charge = async (gold_balance_by_month, gold_req_balance_gram, per_gram_price) => {
    let service_charge = {
        "total_service_charge": 0,
        "balance_of_last_3_month": 0,
        "service_charge_of_last_3_month": 0,
        "service_charge_last_3_month_percentis": 10,
        "balance_of_last_6_month": 0,
        "service_charge_of_last_6_month": 5,
        "service_charge_last_6_month_percentis": 5,
        "balance_of_other": 0,
        "service_charge_other": 0,
        "service_charge_other_percentis": 0,
    }
    // console.log("gold_req_balance_gram" + gold_req_balance_gram);
    if (gold_balance_by_month.balance_other < gold_req_balance_gram) {
        gold_req_balance_gram -= gold_balance_by_month.balance_other;
        service_charge.balance_of_other = gold_balance_by_month.balance_other;
    } else {
        service_charge.balance_of_other = gold_req_balance_gram;
        gold_req_balance_gram = 0;
    }

    if (gold_req_balance_gram > 0) {
        // console.log("gold_req_balance_gram: " + gold_req_balance_gram);
        if (gold_balance_by_month.balance_last_6_month < gold_req_balance_gram) {
            gold_req_balance_gram -= gold_balance_by_month.balance_last_6_month;
            service_charge.balance_of_last_6_month = gold_balance_by_month.balance_last_6_month;
        } else {
            service_charge.balance_of_last_6_month = gold_req_balance_gram;
            gold_req_balance_gram = 0;
        }
    }

    if (gold_req_balance_gram > 0) {
        // console.log("gold_req_balance_gram: " + gold_req_balance_gram);
        if (gold_balance_by_month.balance_of_last_3_month < gold_req_balance_gram) {
            gold_req_balance_gram -= gold_balance_by_month.balance_of_last_3_month;
            service_charge.balance_of_last_3_month = gold_balance_by_month.balance_of_last_3_month;
        } else {
            service_charge.balance_of_last_3_month = gold_req_balance_gram;
            gold_req_balance_gram = 0;
        }
    }

    service_charge.service_charge_of_last_3_month = (service_charge.balance_of_last_3_month * per_gram_price) * (service_charge.service_charge_last_3_month_percentis / 100);
    service_charge.service_charge_of_last_6_month = (service_charge.balance_of_last_6_month * per_gram_price) * (service_charge.service_charge_last_6_month_percentis / 100);

    service_charge.total_service_charge = service_charge.service_charge_of_last_3_month + service_charge.service_charge_of_last_6_month;
    service_charge.total_service_charge = Math.round(service_charge.total_service_charge);

    return service_charge;
};

let jwelleryMakingCharge = async () => {
    return 5;
}

let sendSMS = async (mobile_number = "01671794064", message = "") => {

    return new Promise((resolve, reject) => {
        var options = {
            'method': 'POST',
            'url': 'http://66.45.237.70/api.php?username=vividgallery&password=ASZUEKVX&number=88'+mobile_number+'&message='+message,
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        request(options, function (error, response) {
            if (error) resolve(false);
            resolve(true);
        });
    })
}



module.exports = {
    deleteFile,
    getTodayDateTime,
    getTodayDate,
    getFormatedDateForWebView,
    getCustomDate,
    addFiveMinutes,
    compareTwoDate,
    rendomDigitNumberGenerate,
    hashAString,
    sendEmail,
    getDateTimeFormateForDatabase,
    checkEmailValid,
    compareDataWithHash,
    getVoriTOGram,
    getVATPercentis,
    goldToTakaConvert,
    getCurrentVoryWightInGram,
    gold_available_balance_by_month,
    getGramWidrowLimitInGram,
    calculate_service_charge,
    jwelleryMakingCharge,
    sendSMS
}
